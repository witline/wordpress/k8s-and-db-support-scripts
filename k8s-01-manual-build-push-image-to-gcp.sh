#!/bin/bash
# build-push-image.sh . Build and push a docker image to GCP in a private image repository.

echo
echo "Usage: $0 "
echo "Expects a site-k8s.env file in the project directory."
echo

if [ ! -r ./site-k8s.env ]; then
  echo "./site-k8s.env was not readable. Exiting..."
  exit 1

fi 

source ./site-k8s.env

# Variables:
GCR_IMAGE_NAME=$(echo ${SITE_NAME} | tr '.' '-')

GCP_PROJECT_ID=$(cat ${GCP_PROJECT_ID_FILE})
GCR_IMAGE_URL=eu.gcr.io/${GCP_PROJECT_ID}/${GCR_IMAGE_NAME}

# The GCP credentials file is stored out-of-band in a central location on file system.
GCP_CREDENTIALS=$(cat /home/kamran/Keys-and-Tokens/witline/CI-ACCESS-to-witline-GCP-resources.json)

# Main program:
echo
echo "Building container image: ${GCR_IMAGE_URL} ..."
echo
echo  ${GCP_CREDENTIALS} | docker login -u _json_key --password-stdin https://eu.gcr.io

docker build -t ${GCR_IMAGE_URL}:initial \
             -t ${GCR_IMAGE_URL}:latest  .

docker push ${GCR_IMAGE_URL}:initial
docker push ${GCR_IMAGE_URL}:latest

