#!/bin/bash

echo
echo "Usage: $0 "
echo "Example: $0 "
echo
echo "Expects and uses 'site-k8s.env' file, and generates kubernetes deployment files"
echo





if [ ! -r ./site-k8s.env ]; then
  echo "File ./site-k8s.env could not be read, exiting ..."
  exit 1
else
  echo "Here are the values found in ./site-k8s.env file ..."
  echo
  cat ./site-k8s.env
  echo
  source ./site-k8s.env

fi 


SITE_ID=$(echo ${SITE_NAME} | tr '.' '-')


# Note: 
# SITE_NAME has dots '.' in it's name . Only used in Ingress.
# SITE_ID has '-' in it's name. Used everywhere.

GCP_PROJECT_ID=$(cat ${GCP_PROJECT_ID_FILE})
GCR_IMAGE_NAME=${SITE_ID}

echo "Generating site-deployment.yaml"

# This is (initial) manual deployment, so IMAGE_TAG will simply be "latest".
IMAGE_TAG=latest

sed -e "s/SITE_ID/${SITE_ID}/g" \
    -e "s/IMAGE_TAG/${IMAGE_TAG}/g" \
    -e "s/NAMESPACE/${NAMESPACE}/g" \
    -e "s/REPLICAS/${REPLICAS}/g" \
    -e "s/SITE_NAME/${SITE_NAME}/g" \
    -e "s/GCP_PROJECT_ID/${GCP_PROJECT_ID}/g" \
    -e "s/GCR_IMAGE_NAME/${GCR_IMAGE_NAME}/g" \
    site-deployment.template.yaml > site-deployment.yaml
    
echo "Generating site-pvc.yaml"

sed -e "s/SITE_ID/${SITE_ID}/g" \
    -e "s/NAMESPACE/${NAMESPACE}/g" \
    -e "s/STORAGE_SIZE/${STORAGE_SIZE}/g" \
    site-pvc.template.yaml > site-pvc.yaml

