#!/bin/bash

echo
echo "Usage: $0    <NAMESPACE>"
echo "Example: $0    dev "
echo
echo "This tool deploys kubernetes objects - pvc, deployments, services, ingresses."
echo


NAMESPACE=$1


if [ -z "${NAMESPACE}" ] ; then
  echo "'Namespace' was not provided. Exiting ..."
  echo

  exit 1
else
  echo "Using '${NAMESPACE}' namespace to create kubernetes deployment files ..."
  echo
fi 

kubectl --namespace=${NAMESPACE} apply -f site-pvc.yaml
kubectl --namespace=${NAMESPACE} apply -f site-deployment.yaml
