The files in this directory need to be in a central location.

* The `k8s-*.sh` scripts need to be in `~/bin/` or `/usr/local/bin/` on local work-computer.
* The `mysql-*.sh` scripts would normally go in `/root/bin/` on the old DB server, from where you are migrating your databases to a new DB server. The OS user used to do these operations is normally `root`.


# Steps to deploy a wordpress website

You need to have a environment file containing database host, username and password, which will be used by mysql scripts to create the database on the target db server.

The same environment file is used by the k8s scripts to create kubernetes secret and other objects.

The format (and contents) of this file are:

```
[kamran@kworkhorse k8s-and-db-support-scripts]$ cat app.prod.env.example 
SITE_NAME=example.com
WORDPRESS_DB_HOST=mysql.prod.svc.cluster.local
WORDPRESS_DB_NAME=db_example_com
WORDPRESS_DB_USER=user_example_com
WORDPRESS_DB_PASSWORD=xj1VWw+iS4dXzx63obrfcFqSgy2TgSaH
WORDPRESS_TABLE_PREFIX=wp_
APACHE_RUN_USER=#33
APACHE_RUN_GROUP=#33
GIT_USER=your-git-hub-lab-user
GIT_TOKEN=4Y3WWv67IJE7xx/l0vHEXc+WcalY9r82
```

**Note:** GIT_TOKEN is stored in base 64 encoding.



## Create MySQL database:
```
[kamran@kworkhorse k8s-and-db-support-scripts]$ ls -l mysql-*
-rwxr-xr-x 1 kamran kamran 1504 May 21  2020 mysql-create-db-user-password.sh
-rwxr-xr-x 1 kamran kamran 1385 May 21  2020 mysql-delete-db-user-password.sh
-rwxr-xr-x 1 kamran kamran 1240 May 21  2020 mysql-restore-db.sh
[kamran@kworkhorse k8s-and-db-support-scripts]$ 
```


* It is expected that these (`mysql-*.sh`) scripts will be used on a existing DB server, from which you are migrating away to a DB server elsewhere. This new DB server could be in the cloud, or anywhere.
* To be able to connect to this new DB server, you need to provide credentials of the new DB server in a ".my.cnf-newserver" file.
* Since the source (old) DB server may be operated by the local OS root user, it may already have a ".my.cnf" file to provide password-less / smooth login to the local mysql service.
* Therefore, the credential file (.my.cnf.gcp) for the new mysql server should be stored under OS root user's home directory.
* This way you can smoothly operate both old and new mysql  servers.
* Provide the full path of the mysql credential file file for the new server as MYSQL_DEFAULTS_FILE in the `mysql-*.sh` scripts.

The scripts do the following:
* mysql-create-db-user-password.sh - Create database and user/password on **new** DB server
* mysql-delete-db-user-password.sh - Delete database and user/password on **new** DB server, in case there is a problem with it.
* mysql-restore-db.sh - Restores a database dump from old DB serve to/on the **new** DB server



## Deploy the site on kubernetes:

```
[kamran@kworkhorse example.com]$ ls ~/bin/k8s-0* -l
-rwxr-xr-x 1 kamran kamran 1006 May 29  2020 /home/kamran/bin/k8s-01-manual-build-push-image-to-gcp.sh
-rwxr-xr-x 1 kamran kamran 2195 May 29  2020 /home/kamran/bin/k8s-02-create-secret.sh
-rwxr-xr-x 1 kamran kamran 1300 May 29  2020 /home/kamran/bin/k8s-03-generate-yaml-from-template.sh
-rwxr-xr-x 1 kamran kamran  507 May 29  2020 /home/kamran/bin/k8s-04-kubectl-apply.sh
[kamran@kworkhorse example.com]$ 
```


Steps:
* make copy of a existing wordpress website
* remove .git directory
* create a new git repo in existing directory (git init)
* Fix the app.prod.env 
* create empty database in mysql using credentials from the app.prod.env file 
* create new git repository in gitlab
* disable .gitlab-ci.yml temporarily
* create CI variables in gitlab , GCP_CREDENTIALS from another repo; and SITE_CREDENTIALS using app.prod.env file.
* create secret
* generate yaml file from template, which also creates PVC file
* build and push image to GCR
* create deployment manually 

```
[kamran@kworkhorse example.com]$ k8s-01-manual-build-push-image-to-gcp.sh 

Usage: /home/kamran/bin/k8s-01-manual-build-push-image-to-gcp.sh 
Expects a site-k8s.env file in the project directory.


Building container image: eu.gcr.io/witline/example-com ...

WARNING! Your password will be stored unencrypted in /home/kamran/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Login Succeeded
Sending build context to Docker daemon  91.14kB
Step 1/3 : FROM registry.gitlab.com/wbitt/docker/wordpress:5.6.0-php-7.4-apache-2.4
 ---> 4c03a8d8f8ba
Step 2/3 : COPY themes /usr/src/themes/
 ---> 06f86b324282
Step 3/3 : COPY plugins /usr/src/plugins/
 ---> 554eb2095bb3
Successfully built 554eb2095bb3
Successfully tagged eu.gcr.io/witline/example-com:initial
Successfully tagged eu.gcr.io/witline/example-com:latest
The push refers to repository [eu.gcr.io/witline/example-com]
f19a63896113: Pushed 
6714a1f54852: Pushed 
3224d3b12fe6: Layer already exists 
724df74ae7b9: Layer already exists 
cb42413394c4: Layer already exists 
initial: digest: sha256:012f053b7e2da67a550587fa18880776a6d0875a217f1e4048fe8b1758bc449c size: 5545
The push refers to repository [eu.gcr.io/witline/example-com]
f19a63896113: Layer already exists 
6714a1f54852: Layer already exists 
708ba1e65ea2: Layer already exists 
cb42413394c4: Layer already exists 
latest: digest: sha256:012f053b7e2da67a550587fa18880776a6d0875a217f1e4048fe8b1758bc449c size: 5545
[kamran@kworkhorse example.com]$ 
```



```
[kamran@kworkhorse example.com]$ k8s-02-create-secret.sh prod

Usage: /home/kamran/bin/k8s-02-create-secret.sh <k8s-namespace>
Example: /home/kamran/bin/k8s-02-create-secret.sh   dev

Expects and uses 'site-k8s.env' file, and generates secret with the name credentials-<site-tld>

Here are the values found in ./site-k8s.env file ...

# This variable contains the project ID of the GCP project:
GCP_PROJECT_ID_FILE=/home/kamran/Keys-and-Tokens/witline/GCP-project-id.txt

# This token needs to be created in GCP, and downloaded to local computer in a central location:
GCP_CREDENTIALS_FILE=/home/kamran/Keys-and-Tokens/witline/CI-ACCESS-to-witline-GCP-resources.json

# Name of the website being migrated to k8s:
SITE_NAME=example.com

# The environment file which contains all the secrets used by wordpress.
# This needs to be separate from app.env , because the DNS name of the DB server is different on k8s.
ENV_FILE=app.prod.env

# How large PVC is required by this wordpress website
STORAGE_SIZE=1Gi


Namespace was found set to 'prod'. Using 'prod' namespace to create secret ...

SECRET_NAME was found set to 'credentials-example-com'. Using 'credentials-example-com' as name of the secret ...


First deleting the old secret: 'credentials-example-com' from the 'prod' namespace ...
Error from server (NotFound): secrets "credentials-example-com" not found

Found 'app.prod.env' file, creating kubernetes secret: 'credentials-example-com' in the 'prod' namespace ...
secret/credentials-example-com created

[kamran@kworkhorse example.com]$ 
```


```
[kamran@kworkhorse example.com]$ k8s-03-generate-yaml-from-template.sh 

Usage: /home/kamran/bin/k8s-03-generate-yaml-from-template.sh 
Example: /home/kamran/bin/k8s-03-generate-yaml-from-template.sh 

Expects and uses 'site-k8s.env' file, and generates kubernetes deployment files

Here are the values found in ./site-k8s.env file ...

# This variable contains the project ID of the GCP project:
GCP_PROJECT_ID_FILE=/home/kamran/Keys-and-Tokens/witline/GCP-project-id.txt

# This token needs to be created in GCP, and downloaded to local computer in a central location:
GCP_CREDENTIALS_FILE=/home/kamran/Keys-and-Tokens/witline/CI-ACCESS-to-witline-GCP-resources.json

# Name of the website being migrated to k8s:
SITE_NAME=example.com

# The environment file which contains all the secrets used by wordpress.
# This needs to be separate from app.env , because the DNS name of the DB server is different on k8s.
ENV_FILE=app.prod.env

# How large PVC is required by this wordpress website
STORAGE_SIZE=1Gi


Generating site-deployment.yaml
Generating site-pvc.yaml
[kamran@kworkhorse example.com]$ 
```


```
[kamran@kworkhorse example.com]$ k8s-04-kubectl-apply.sh prod

Usage: /home/kamran/bin/k8s-04-kubectl-apply.sh    <NAMESPACE>
Example: /home/kamran/bin/k8s-04-kubectl-apply.sh    dev 

This tool deploys kubernetes objects - pvc, deployments, services, ingresses.

Using 'prod' namespace to create kubernetes deployment files ...

persistentvolumeclaim/pvc-example-com unchanged
deployment.apps/example-com configured
service/example-com unchanged
Warning: extensions/v1beta1 Ingress is deprecated in v1.14+, unavailable in v1.22+; use networking.k8s.io/v1 Ingress
ingress.extensions/example-com configured
[kamran@kworkhorse example.com]$
```
