#!/bin/bash
# This script reads app.prod.env and creates equivalent kubernetes secrets.
# It needs to be capable for creating k8s secrets by reading ENV variables as well,
#   as, that is the case with CI systems.


echo
echo "Usage: $0 <k8s-namespace>"
echo "Example: $0   dev"
echo
echo "Expects and uses 'site-k8s.env' file, and generates secret with the name credentials-<site-tld>"
echo


if [ ! -r ./site-k8s.env ]; then
  echo "File ./site-k8s.env could not be read, exiting ..."
  exit 1
else
  echo "Here are the values found in ./site-k8s.env file ..."
  echo
  cat ./site-k8s.env
  echo
fi 


source ./site-k8s.env



NS=$1

SECRET_NAME=credentials-$(echo ${SITE_NAME} | tr '.' '-')

# ENV_FILE variable is read from ./site-k8s.env file.

if [ ! -f ${ENV_FILE} ]; then
  echo "Could not find ENV variables file ${ENV_FILE}. Exiting ..."
  echo
  exit 1
fi


if [ -z "${NS}" ] ; then
  echo "Namespace name was not provided. Exiting ..."
  echo

  exit 1
else
  echo "Namespace was found set to '${NS}'. Using '${NS}' namespace to create secret ..."
  echo
  NS_OPTION="--namespace=${NS}"
fi 


if [ -z "${SECRET_NAME}" ] ; then
  echo "Name of the secret being created is required. Exiting ..."
  echo

  exit 1
else
  echo "SECRET_NAME was found set to '${SECRET_NAME}'. Using '${SECRET_NAME}' as name of the secret ..."
  echo
fi 


echo
echo "First deleting the old secret: '${SECRET_NAME}' from the '${NS}' namespace ..."
kubectl ${NS_OPTION} delete secret ${SECRET_NAME}  || true

echo
echo "Found '${ENV_FILE}' file, creating kubernetes secret: '${SECRET_NAME}' in the '${NS}' namespace ..."
source ${ENV_FILE}

kubectl ${NS_OPTION} create secret generic ${SECRET_NAME} \
  --from-literal=WORDPRESS_DB_HOST=${WORDPRESS_DB_HOST} \
  --from-literal=WORDPRESS_DB_NAME=${WORDPRESS_DB_NAME} \
  --from-literal=WORDPRESS_DB_USER=${WORDPRESS_DB_USER} \
  --from-literal=WORDPRESS_DB_PASSWORD=${WORDPRESS_DB_PASSWORD} \
  --from-literal=WORDPRESS_TABLE_PREFIX=${WORDPRESS_TABLE_PREFIX} \
  --from-literal=APACHE_RUN_USER=${APACHE_RUN_USER} \
  --from-literal=APACHE_RUN_GROUP=${APACHE_RUN_GROUP} \
  --from-literal=GIT_USER=${GIT_USER} \
  --from-literal=GIT_TOKEN=${GIT_TOKEN}

echo


