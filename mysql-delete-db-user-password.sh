#!/bin/bash
# WARNING: This script deletes database and user on the server specified
#          in the /root/.my.cnf.gcp.

# * It is expected that this script will be used on a existing DB server,
#     from which you are migrating away to a DB server elsewhere. This 
#     new DB server could be in the cloud, or anywhere.
# * To be able to connect to this new DB server, you need to provide
#     credentials of the new DB server in a ".my.cnf-newserver" file.
# * Since the source (old) DB server may be operated by the local OS root
#     user, it may already have a ".my.cnf" file to provide password-less
#     / smooth login to the local mysql service.
# * Therefore, the credential file (.my.cnf.gcp) for the new mysql server
#     should be stored under OS root user's home directory.
# * This way you can smoothly operate both old and new mysql  servers.
# * Provide the full path of the mysql credential file file for the new
#     server as MYSQL_DEFAULTS_FILE - below.

MYSQL_DEFAULTS_FILE=/root/.my.cnf.gcp

if [ -z "$1" ]; then
  echo "Need name of site db details file, e.g. app.prod.env . Exiting ..."
  exit 1
fi

SITE_DB_DETAILS_FILE=$1

if [ ! -r ${SITE_DB_DETAILS_FILE} ]; then
  echo "The file provided cannot be read. Exiting..."
  exit 1
fi 

if [ ! -r ${MYSQL_DEFAULTS_FILE} ]; then
  echo "The MYSQL DEFAULTS FILE could not be read. Exiting..."
  exit 1
fi 


# Read the MySQL variables from the local directory.
source ${SITE_DB_DETAILS_FILE}
echo
echo "Found: WORDPRESS_DB_NAME=${WORDPRESS_DB_NAME}"
echo "Found: WORDPRESS_DB_USER=${WORDPRESS_DB_USER}"
echo "Found: WORDPRESS_DB_PASSWORD=---REDACTED---"
# echo "Found: WORDPRESS_DB_PASSWORD=${WORDPRESS_DB_PASSWORD}"
echo

echo "Proceeding to DROP database and user , using --defaults-file=${MYSQL_DEFAULTS_FILE} ..."
echo
mysql --defaults-file=${MYSQL_DEFAULTS_FILE} -e "DROP USER ${WORDPRESS_DB_USER}@'%' ;"
mysql --defaults-file=${MYSQL_DEFAULTS_FILE} -e "DROP DATABASE ${WORDPRESS_DB_NAME} ;"

# mysql --defaults-file=${MYSQL_DEFAULTS_FILE} -e "DROP USER ${WORDPRESS_DB_USER}@'%' IDENTIFIED BY '${WORDPRESS_DB_PASSWORD}';"
# mysql --defaults-file=${MYSQL_DEFAULTS_FILE} -e "GRANT ALL PRIVILEGES ON ${WORDPRESS_DB_NAME}.* TO '${WORDPRESS_DB_USER}'@'%';"
mysql --defaults-file=${MYSQL_DEFAULTS_FILE} -e "FLUSH PRIVILEGES;"
