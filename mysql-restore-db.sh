#!/bin/bash

# This script restores database on the server specified
#          in the /root/.my.cnf.gcp.
echo
echo "This script restores a database in MySQL in the new DB server,"
echo "  specified in the /root/.my.cnf.gcp file."
echo
echo "Usage: $0 <sitename.app.prod.env>  </path/to/db.dump>"
echo
echo "The dump file must be uncompressed beforehand ."
echo

# * It is expected that this script will be used on a existing DB server,
#     from which you are migrating away to a DB server elsewhere. This 
#     new DB server could be in the cloud, or anywhere.
# * To be able to connect to this new DB server, you need to provide
#     credentials of the new DB server in a ".my.cnf-newserver" file.
# * Since the source (old) DB server may be operated by the local OS root
#     user, it may already have a ".my.cnf" file to provide password-less
#     / smooth login to the local mysql service.
# * Therefore, the credential file (.my.cnf.gcp) for the new mysql server
#     should be stored under OS root user's home directory.
# * This way you can smoothly operate both old and new mysql  servers.
# * Provide the full path of the mysql credential file file for the new
#     server as MYSQL_DEFAULTS_FILE - below.

MYSQL_DEFAULTS_FILE=/root/.my.cnf.gcp

SITE_DB_DETAILS_FILE=$1
DUMP_FILE_PATH=$2

if [ -z "${SITE_DB_DETAILS_FILE}" ]; then
  echo "Need name of site db details file, e.g. sitename.app.prod.env . Exiting ..."
  exit 1
fi

if [ -z "${DUMP_FILE_PATH}" ]; then
  echo "Need full path of the dumpfile for this domain/website .e.g site.dump . Exiting ..."
  exit 1
fi


if [ ! -r ${SITE_DB_DETAILS_FILE} ]; then
  echo "The file provided (${SITE_DB_DETAILS_FILE}) cannot be read. Exiting..."
  exit 1
fi 

if [ ! -r ${MYSQL_DEFAULTS_FILE} ]; then
  echo "The MYSQL DEFAULTS FILE could not be read. Exiting..."
  exit 1
fi 


# Read the MySQL variables from the local directory.
source ${SITE_DB_DETAILS_FILE}
echo
echo "Found: WORDPRESS_DB_NAME=${WORDPRESS_DB_NAME}"
#echo "Found: WORDPRESS_DB_USER=${WORDPRESS_DB_USER}"
#echo "Found: WORDPRESS_DB_PASSWORD=******************"
echo

echo "Proceeding to restore database dump ${DUMP_FILE_PATH} ..."
echo
mysql --defaults-file=${MYSQL_DEFAULTS_FILE} --database=${WORDPRESS_DB_NAME} < ${DUMP_FILE_PATH}
